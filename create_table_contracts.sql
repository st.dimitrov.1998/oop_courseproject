USE rental_system;

DROP TABLE IF EXISTS contracts;

CREATE TABLE contracts(
	`contract_id` int NOT NULL AUTO_INCREMENT,
    `operator_id` int NOT NULL,
    `client_pid` varchar(10) NOT NULL,
    `vin` varchar(17) NOT NULL,
    `protocol_id` int NOT NULL,
    `date_of_rent` datetime DEFAULT NULL,
    `due_date` date DEFAULT NULL,
    `date_of_return` datetime DEFAULT NULL,
    PRIMARY KEY(`contract_id`),
    FOREIGN KEY(`operator_id`) REFERENCES users(`user_id`),
    FOREIGN KEY(`client_pid`) REFERENCES clients(`client_pid`),
    FOREIGN KEY(`vin`) REFERENCES vehicles(`vin`),
    FOREIGN KEY(`protocol_id`) REFERENCES protocols(`protocol_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;