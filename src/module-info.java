module rentalSystem {
	requires java.sql;
	requires java.persistence;
	requires org.hibernate.orm.core;
	
	requires javafx.fxml;
	requires javafx.controls;
	requires javafx.graphics;
	requires javafx.web;
	requires javafx.base;
	
	opens rentalSystem;
}