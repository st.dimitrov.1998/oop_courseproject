package rentalSystem;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

public class Validations {
	public static boolean isValidInt(TextField field) {
    	if(field.getText() == null) {
    		field.setStyle("-fx-border-color: red;");
    		return false;
    	}
    	try {
    		if(Integer.parseInt(field.getText()) < 0) {
    			Alert alert = new Alert(AlertType.ERROR, "Number can't be negative!", ButtonType.OK);
        		field.setStyle("-fx-border-color: red;");
    			alert.showAndWait();
    			field.setText("");
        		return false;
    		}
    	} catch(NumberFormatException e){
    		Alert alert = new Alert(AlertType.ERROR, "Invalid input!", ButtonType.OK);
    		field.setStyle("-fx-border-color: red;");
    		alert.showAndWait();
			field.setText("");
    		return false;
    	}
    	field.setStyle("-fx-border-color: none;");
    	return true;
    }
    
    public static boolean isValidFloat(TextField field) {
    	if(field.getText() == null) {
    		field.setStyle("-fx-border-color: red;");
    		return false;
    	}
    	try {
    		if(Float.parseFloat(field.getText()) < 0) {
    			Alert alert = new Alert(AlertType.ERROR, "Number can't be negative!", ButtonType.OK);
        		field.setStyle("-fx-border-color: red;");
        		alert.showAndWait();
    			field.setText("");
        		return false;
    		}
    	} catch(NumberFormatException e){
    		Alert alert = new Alert(AlertType.ERROR, "Invalid input!", ButtonType.OK);
    		field.setStyle("-fx-border-color: red;");
    		alert.showAndWait();
			field.setText("");
    		return false;
    	}
    	field.setStyle("-fx-border-color: none;");
    	return true;
    }

    public static boolean isNonNumeric(TextField field) {
    	if(field.getText() == null) {
    		field.setStyle("-fx-border-color: red;");
    		return false;
    	}
    	else if(!field.getText().matches("\\A\\D*\\Z")) {
    		Alert alert = new Alert(AlertType.ERROR, "Input should not contain digits!", ButtonType.OK);
    		field.setStyle("-fx-border-color: red;");
    		alert.showAndWait();
			field.setText("");
    		return false;
    	}
    	field.setStyle("-fx-border-color: none;");
    	return true;
    }
    
    public static boolean isValidYear(TextField field) {
    	if(field.getText() == null) {
    		field.setStyle("-fx-border-color: red;");
    		return false;
    	}
    	try {
			int year = Integer.parseInt(field.getText());
			if(year < 1900) {
				Alert alert = new Alert(AlertType.ERROR, "The year entered is too far back! Please enter a year after 1900.", ButtonType.OK);
	    		field.setStyle("-fx-border-color: red;");
	    		alert.showAndWait();
    			field.setText("");
	    		return false;
			}	
		} catch(NumberFormatException e) {
			Alert alert = new Alert(AlertType.ERROR, "Invalid input!", ButtonType.OK);
    		field.setStyle("-fx-border-color: red;");
    		alert.showAndWait();
			field.setText("");
    		return false;
    	}
		field.setStyle("-fx-border-color: none;");
		return true;
    }

    public static boolean isValidVIN(TextField field) {
    	if(field.getText() == null) {
    		field.setStyle("-fx-border-color: red;");
    		return false;
    	}
    	else if(!Vehicle.validateVIN(field.getText())) {
			Alert alert = new Alert(AlertType.ERROR, "Invalid VIN!", ButtonType.OK);
    		field.setStyle("-fx-border-color: red;");
    		alert.showAndWait();
			field.setText("");
    		return false;
		}
		field.setStyle("-fx-border-color: none;");
		return true;
	}
    
    public static boolean isValidLicense(TextField field) {
    	if(field.getText() == null) {
    		field.setStyle("-fx-border-color: red;");
    		return false;
    	}
    	else if(!field.getText().toUpperCase().matches("[A-Z]{1,2}\\s[0-9]{4}")) {
    		Alert alert = new Alert(AlertType.ERROR, "Invalid license plate!", ButtonType.OK);
    		field.setStyle("-fx-border-color: red;");
    		alert.showAndWait();
			field.setText("");
    		return false;
		}
    	field.setStyle("-fx-border-color: none;");
		return true;
	}
    
    public static boolean isValidEmail(TextField field) {
    	if(field.getText() == null) {
    		field.setStyle("-fx-border-color: red;");
    		return false;
    	}
    	else if(!field.getText().contains("@")){
			Alert alert = new Alert(AlertType.ERROR, "Invalid e-mail!", ButtonType.OK);
    		field.setStyle("-fx-border-color: red;");
    		alert.showAndWait();
			field.setText("");
    		return false;
		}
		field.setStyle("-fx-border-color: none;");
		return true;
	}
    
    public static boolean isValidPID(TextField field) {
    	if(field.getText() == null) {
    		field.setStyle("-fx-border-color: red;");
    		return false;
    	}
    	else if(!Client.validatePID(field.getText())) {
    		Alert alert = new Alert(AlertType.ERROR, "Invalid PID!", ButtonType.OK);
    		field.setStyle("-fx-border-color: red;");
    		alert.showAndWait();
			field.setText("");
    		return false;
		}
		field.setStyle("-fx-border-color: none;");
		return true;
    }
    
}
