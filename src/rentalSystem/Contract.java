package rentalSystem;


import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="contracts")
public class Contract {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="contract_id")
	private int contractId;
	
	@ManyToOne
    @JoinColumn(name = "operator_id")
	private Operator operator;
	
	@ManyToOne
    @JoinColumn(name = "client_pid")
	private Client client;
	
	@ManyToOne
    @JoinColumn(name = "vin")
	private Vehicle vehicle;
	
	@OneToOne
    @JoinColumn(name = "protocol_id")
	private Protocol protocol;
	
	@Column(name="date_of_rent")
	private LocalDateTime dateOfRent;
	
	@Column(name="due_date")
	private LocalDate dueDate;
	
	@Column(name="date_of_return")
	private LocalDateTime dateOfReturn;
	
	@Column(name="total_cost")
	private float totalCost;

	public Contract() {
		
	}

	public Contract(Operator operator, Client client, Vehicle vehicle, Protocol protocol, LocalDate dueDate) {
		this.operator = operator;
		this.client = client;
		this.vehicle = vehicle;
		this.protocol = protocol;
		this.dateOfRent = LocalDateTime.now();
		this.dueDate = dueDate;
		this.dateOfReturn = null;
		this.totalCost = 0;
	}
	
	public String toString() {
		String returnDate;
		if(dateOfReturn == null)
			returnDate = "-not returned yet-";
		else
			returnDate = dateOfReturn.toString();
		return vehicle.getVin() + " | " + vehicle.getMake() + " " + vehicle.getModel() + ", Rented to " + client.getFullName() + " by operator " + operator.getFullName() + "(ID:" + operator.getId() + "), rented on: " + dateOfRent.toString() + ", due date: " + dueDate.toString() + ", returned on: " + returnDate;
	}
	
	public int getContractId() {
		return contractId;
	}

	public void setContractId(int contractId) {
		this.contractId = contractId;
	}

	public Operator getOperator() {
		return operator;
	}

	public void setOperator(Operator operator) {
		this.operator = operator;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public Protocol getProtocol() {
		return protocol;
	}

	public void setProtocol(Protocol protocol) {
		this.protocol = protocol;
	}

	public LocalDateTime getDateOfRent() {
		return dateOfRent;
	}

	public void setDateOfRent(LocalDateTime dateOfRent) {
		this.dateOfRent = dateOfRent;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}

	public LocalDateTime getDateOfReturn() {
		return dateOfReturn;
	}

	public void setDateOfReturn(LocalDateTime dateOfReturn) {
		this.dateOfReturn = dateOfReturn;
	}
	
	public float getTotalCost() {
		return totalCost;
	}
	
	public void setTotalCost(float totalCost) {
		this.totalCost = totalCost;
	}
}