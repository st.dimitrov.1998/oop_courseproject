package rentalSystem;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

public class LoginController {
	@FXML
	public TextField userId;
	@FXML
	public PasswordField userPass;
		
	@FXML
	public void logIn() throws Exception{
		if(userId.getText().isBlank() || userPass.getText().isBlank()) {
			Alert alert = new Alert(AlertType.ERROR, "Please fill all fields!", ButtonType.OK);
			alert.show();
    		userId.clear();
    		userPass.clear();
    	}
		else if(!Validations.isValidInt(userId)){
    		userId.clear();
			userPass.clear();
    	}
		else {
    		SessionFactory factory = new Configuration()
					.configure()
					.addAnnotatedClass(Admin.class)
					.addAnnotatedClass(Operator.class)
					.buildSessionFactory();
    		
    		Session session = factory.getCurrentSession();
    		
    		int id = Integer.parseInt(userId.getText());
    		String pass = userPass.getText();
    		userId.clear();
    		userPass.clear();
    		try {
    			session.beginTransaction();
    			if(id == 0) {
    				Admin user = session.get(Admin.class, id);
    				user.logIn(pass);
    			}
    			else if(session.get(Operator.class, id) != null){
    				Operator user = session.get(Operator.class, id);
    				user.logIn(pass);
    			}
    			else {
    				Alert alert = new Alert(AlertType.ERROR, "User not found!", ButtonType.OK);
    				alert.show();
    			}
    				
    			session.getTransaction().commit();
    		} finally {
    			factory.close();
    		}
    	}
	}
}
