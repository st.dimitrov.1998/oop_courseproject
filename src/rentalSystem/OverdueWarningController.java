package rentalSystem;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

public class OverdueWarningController implements Initializable {
	@FXML
	public ListView<String> contractList;
	
	public List<Contract> contracts;
	public void initData(List<Contract> contracts) {
		this.contracts = contracts;
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Platform.runLater(() -> {
			for(Contract c : contracts) {
				contractList.getItems().add(c.getContractId()+" | "+c.getVehicle().getMake()+" "+c.getVehicle().getModel()+" | "+c.getClient().getFullName()+", "+c.getClient().getPhoneNumber());
			}
		});
	}
	
	public void okClick() {
		Stage stage = (Stage)contractList.getScene().getWindow();
		stage.close();
	}
}
