package rentalSystem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="vehicles")
public class Vehicle {	
	@Id
	@Column(name="vin")
	private String vin;
	
	@Column(name="license_plate")
	private String licensePlate;
	
	@Column(name="available")
	private boolean available;
	
	@Column(name="current_state", columnDefinition = "enum('Excellent','Good','Fair','Poor')")
	@Enumerated(EnumType.STRING)
	private VehicleStates currentState;
	
	@Column(name="make")
	private String make;
	
	@Column(name="model")
	private String model;
	
	@Column(name="year")
	private int year;
	
	@Column(name="class", columnDefinition =  "enum('Luxurious','Sport','Cruiser','Family')")
	@Enumerated(EnumType.STRING)
	private VehicleClasses vehicleClass;
	
	@Column(name="category", columnDefinition =  "enum('Convertible','Coupe','Estate','Hatchback','Minivan','Pickup','Roadster','Sedan','SUV','Targa')")
	@Enumerated(EnumType.STRING)
	private VehicleCategories vehicleCategory;
	
	@Column(name="colour")
	private String colour;
	
	@Column(name="engine_size")
	private float engineSize;
	
	@Column(name="horsepower")
	private int horsepower;
	
	@Column(name="fuel", columnDefinition =  "enum('Gasoline','Diesel','Propane','CNG','Ethanol','Electric')")
	@Enumerated(EnumType.STRING)
	private VehicleFuels vehicleFuel;
	
	@Column(name="mileage")
	private int mileage;
	
	@Column(name="smoking_allowed")
	private boolean smokingAllowed;
	
	public Vehicle() {
		
	}

	public Vehicle(String vin, String licensePlate, VehicleStates currentState, String make,
			String model, int year, VehicleClasses vehicleClass, VehicleCategories vehicleCategory, String colour,
			float engineSize, int horsepower, VehicleFuels vehicleFuel, int mileage, boolean smokingAllowed) {
		this.vin = vin.toUpperCase();
		this.licensePlate = licensePlate.toUpperCase();
		if(currentState == VehicleStates.Poor)
			this.available = false;
		else
			this.available = true;	
		this.currentState = currentState;
		this.make = make;
		this.model = model;
		this.year = year;
		this.vehicleClass = vehicleClass;
		this.vehicleCategory = vehicleCategory;
		this.colour = colour;
		this.horsepower = horsepower;
		this.vehicleFuel = vehicleFuel;
		if(vehicleFuel == VehicleFuels.Electric)
			this.engineSize = 0;
		else 
			this.engineSize = engineSize;
		this.mileage = mileage;
		this.smokingAllowed = smokingAllowed;
	}
	
	public String toString() {
		String smoking;
		if(smokingAllowed)
			smoking = "smoking allowed";
		else smoking = "no smoking allowed";
		return vin + " | " + licensePlate + " | " + make + " " + model + ", " + year + ", " + vehicleClass + " " + vehicleCategory + ", " + colour + ", " + engineSize + "L " + vehicleFuel + ", " + horsepower + "hp, " + smoking;
	}
	
	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

	public VehicleStates getCurrentState() {
		return currentState;
	}

	public void setCurrentState(VehicleStates currentState) {
		this.currentState = currentState;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public VehicleClasses getVehicleClass() {
		return vehicleClass;
	}

	public void setVehicleClass(VehicleClasses vehicleClass) {
		this.vehicleClass = vehicleClass;
	}

	public VehicleCategories getVehicleCategory() {
		return vehicleCategory;
	}

	public void setVehicleCategory(VehicleCategories vehicleCategory) {
		this.vehicleCategory = vehicleCategory;
	}

	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public float getEngineSize() {
		return engineSize;
	}

	public void setEngineSize(float engineSize) {
		this.engineSize = engineSize;
	}

	public int getHorsepower() {
		return horsepower;
	}

	public void setHorsepower(int horsepower) {
		this.horsepower = horsepower;
	}

	public VehicleFuels getVehicleFuel() {
		return vehicleFuel;
	}

	public void setVehicleFuel(VehicleFuels vehicleFuel) {
		this.vehicleFuel = vehicleFuel;
	}

	public int getMileage() {
		return mileage;
	}

	public void setMileage(int mileage) {
		this.mileage = mileage;
	}
	
	public void addMiles(int miles) {
		this.mileage += miles;
	}

	public boolean isSmokingAllowed() {
		return smokingAllowed;
	}

	public void setSmokingAllowed(boolean smokingAllowed) {
		this.smokingAllowed = smokingAllowed;
	}

	public static boolean validateVIN(String vin) {
		vin = vin.toUpperCase();
		if(vin.length() == 17 && vin.matches("[A-HJ-NPR-Z0-9]{17}")) {
			int ctrl = 0;
			int[] weights = { 8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2 };
			int value;
			for(int i = 0; i < vin.length(); i++) {
				switch(vin.charAt(i)) {
				case 'A':
				case 'J':
					value = 1;
					break;
				case 'B':
				case 'K':
				case 'S':
					value = 2;
					break;
				case 'C':
				case 'L':
				case 'T':
					value = 3;
					break;
				case 'D':
				case 'M':
				case 'U':
					value = 4;
					break;
				case 'E':
				case 'N':
				case 'V':
					value = 5;
					break;
				case 'F':
				case 'W':
					value = 6;
					break;
				case 'G':
				case 'P':
				case 'X':
					value = 7;
					break;
				case 'H':
				case 'Y':
					value = 8;
					break;
				case 'R':
				case 'Z':
					value = 9;
					break;
				default:
					value = vin.charAt(i) - '0';
				}
				ctrl += weights[i] * value;
			}
			
			ctrl %= 11;
			if(ctrl == vin.charAt(8) || (ctrl == 10 && vin.charAt(8) == 'X'))
				return true;
			else
				return false;
		}
		else 
			return false;
	}
}
