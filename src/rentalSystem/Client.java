package rentalSystem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.time.Year;

@Entity
@Table(name="clients")
public class Client {
	@Id
	@Column(name="client_pid")
	private String pid;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="phone_number")
	private String phoneNumber;
	
	@Column(name="address")
	private String address;
	
	@Column(name="billing_info")
	private String billingInfo;
	
	@Column(name="rating")
	private float rating;

	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFullName() {
		return firstName+" "+lastName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getBillingInfo() {
		return billingInfo;
	}
	public void setBillingInfo(String billingInfo) {
		this.billingInfo = billingInfo;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	
	public void lowerRating() {
		this.rating -= 0.75f;
	}
	
	public void raiseRating() {
		this.rating += 0.25f;
	}

	public Client() {
	}
	
	public Client(String pid, String firstName, String lastName, String phoneNumber, String address, String billingInfo) {
		this.pid = pid;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.billingInfo = billingInfo;
		this.rating = 5;
		System.out.println("Client registered.");
	}
	
	public String toString() {
		return pid + " | " + firstName + " " + lastName + ", " + rating + ", Personal info: [Tel.: " + phoneNumber + ", Address: " + address + ", IBAN:" + billingInfo + "]";
	}
		
	public static boolean validatePID(String pid) {
		if(pid.length() == 10 && pid.matches("\\d+")) {
			int day = Integer.parseInt(pid.substring(4, 6));
			int month = Integer.parseInt(pid.substring(2, 4));
			if(1 <= month && month <= 12) {
				int year = 1900 + Integer.parseInt(pid.substring(0, 2));
				switch(month) {
				case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                	if(!(1 <= day && day <= 31))
                		return false;
                	break;
                case 4:
                case 6:
                case 9:
                case 11:
                	if(!(1 <= day && day <= 30))
                		return false;
                	break;
                case 2:
                	if(!((1 <= day && day <= 28) || (day == 29 && Year.isLeap(year))))
                		return false;
                	break;
				}
				int ctrl = 0;
				int[] ctrlValues = { 2, 4, 8, 5, 10, 9, 7, 3, 6 };
				for(int i = 0; i < ctrlValues.length; i++) {
					ctrl += ctrlValues[i] * (pid.charAt(i) - '0');
				}
				ctrl %= 11;
				if(ctrl % 10 != (pid.charAt(9) - '0'))
					return false;
				else 
					return true;
			}
			else if(21 <= month && month <= 32) {
				int year = 1800 + Integer.parseInt(pid.substring(0, 2));
				switch(month) {
				case 21:
                case 23:
                case 25:
                case 27:
                case 28:
                case 30:
                case 32:
                	if(!(1 <= day && day <= 31))
                		return false;
                	break;
                case 24:
                case 26:
                case 29:
                case 31:
                	if(!(1 <= day && day <= 30))
                		return false;
                	break;
                case 22:
                	if(!((1 <= day && day <= 28) || (day == 29 && Year.isLeap(year))))
                		return false;
                	break;
				}
				int ctrl = 0;
				int[] ctrlValues = { 2, 4, 8, 5, 10, 9, 7, 3, 6 };
				for(int i = 0; i < ctrlValues.length; i++) {
					ctrl += ctrlValues[i] * (pid.charAt(i) - '0');
				}
				ctrl %= 11;
				if(ctrl % 10 != (pid.charAt(9) - '0'))
					return false;
				else 
					return true;
			}
			else if(41 <= month && month <= 52) {
				int year = 2000 + Integer.parseInt(pid.substring(0, 2));
				switch(month) {
				case 41:
                case 43:
                case 45:
                case 47:
                case 48:
                case 50:
                case 52:
                	if(!(1 <= day && day <= 31))
                		return false;
                	break;
                case 44:
                case 46:
                case 49:
                case 51:
                	if(!(1 <= day && day <= 30))
                		return false;
                	break;
                case 42:
                	if(!((1 <= day && day <= 28) || (day == 29 && Year.isLeap(year))))
                		return false;
                	break;
				}
				int ctrl = 0;
				int[] ctrlValues = { 2, 4, 8, 5, 10, 9, 7, 3, 6 };
				for(int i = 0; i < ctrlValues.length; i++) {
					ctrl += ctrlValues[i] * (pid.charAt(i) - '0');
				}
				ctrl %= 11;
				if(ctrl % 10 != (pid.charAt(9) - '0'))
					return false;
				else 
					return true;
			}
			else
				return false;
		}
		else
			return false;
	}
}