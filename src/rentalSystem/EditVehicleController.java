package rentalSystem;

import java.net.URL;
import java.util.ResourceBundle;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;;

public class EditVehicleController implements Initializable {
	@FXML
	public TextField vin;
	@FXML
	public TextField plate;
	@FXML
	public ComboBox<VehicleStates> state;
	@FXML
	public TextField make;
	@FXML
	public TextField model;
	@FXML
	public TextField year;
	@FXML
	public ComboBox<VehicleClasses> vClass;
	@FXML
	public ComboBox<VehicleCategories> category;
	@FXML
	public TextField colour;
	@FXML
	public TextField displ;
	@FXML
	public TextField power;
	@FXML
	public ComboBox<VehicleFuels> fuel;
	@FXML
	public TextField mileage;
	@FXML
	public CheckBox smokingAllowed;
	
	public Admin user;
	public String vehicleID;
	
	public void initData(Admin user, String vin) {
		this.user = user;
		this.vehicleID = vin;
	}
	
	public void submitClick() throws Exception {
		if(!Validations.isValidLicense(plate) || !Validations.isNonNumeric(make) || model.getText().isBlank() || !Validations.isValidYear(year)
				|| !Validations.isNonNumeric(colour) || !Validations.isValidFloat(displ) || !Validations.isValidInt(power) 
				|| !Validations.isValidInt(mileage) || state.getSelectionModel().isEmpty() || vClass.getSelectionModel().isEmpty() 
				|| category.getSelectionModel().isEmpty() || fuel.getSelectionModel().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR, "Please fill all fields!", ButtonType.OK);
			alert.show();
		}
		else {
			user.editVehicle(vin.getText(), plate.getText(), state.getSelectionModel().getSelectedItem(), make.getText(), model.getText(), 
					Integer.parseInt(year.getText()), vClass.getSelectionModel().getSelectedItem(), category.getSelectionModel().getSelectedItem(), 
					colour.getText(), Float.parseFloat(displ.getText()), Integer.parseInt(power.getText()), fuel.getSelectionModel().getSelectedItem(),
					Integer.parseInt(mileage.getText()), smokingAllowed.isSelected());
			Stage currentStage = (Stage)vin.getScene().getWindow();
			currentStage.close();
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Vehicle.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			Vehicle vehicle = session.get(Vehicle.class, vehicleID);
			session.getTransaction().commit();
			
			vin.setText(vehicle.getVin());
			vin.setEditable(false);
			
			plate.setText(vehicle.getLicensePlate());
			
			state.getItems().addAll(VehicleStates.Excellent,
					VehicleStates.Good,
					VehicleStates.Fair,
					VehicleStates.Poor);
			state.getSelectionModel().select(vehicle.getCurrentState());
			
			make.setText(vehicle.getMake());
			
			model.setText(vehicle.getModel());
			
			year.setText(Integer.toString(vehicle.getYear()));
			
			vClass.getItems().addAll(VehicleClasses.Luxurious,
					VehicleClasses.Sport,
					VehicleClasses.Cruiser,
					VehicleClasses.Family);
			vClass.getSelectionModel().select(vehicle.getVehicleClass());
			
			category.getItems().addAll(VehicleCategories.Convertible,
					VehicleCategories.Coupe,
					VehicleCategories.Estate,
					VehicleCategories.Hatchback,
					VehicleCategories.Minivan,
					VehicleCategories.Pickup,
					VehicleCategories.Roadster,
					VehicleCategories.Sedan,
					VehicleCategories.SUV,
					VehicleCategories.Targa);
			category.getSelectionModel().select(vehicle.getVehicleCategory());
			
			colour.setText(vehicle.getColour());
			
			displ.setText(Float.toString(vehicle.getEngineSize()));
			
			power.setText(Integer.toString(vehicle.getHorsepower()));
			
			fuel.getItems().addAll(VehicleFuels.Gasoline,
					VehicleFuels.Diesel,
					VehicleFuels.Propane,
					VehicleFuels.CNG,
					VehicleFuels.Ethanol,
					VehicleFuels.Electric);
			fuel.getSelectionModel().select(vehicle.getVehicleFuel());
			
			mileage.setText(Integer.toString(vehicle.getMileage()));
			
			smokingAllowed.setSelected(vehicle.isSmokingAllowed());
		} finally {
			factory.close();
		}
	}
}
