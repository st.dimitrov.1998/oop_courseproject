package rentalSystem;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.ResourceBundle;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class ReturnCarController2 implements Initializable {
	@FXML
	public TextArea firstProtocol;
	@FXML
	public TextArea secondProtocol;
	@FXML
	public ComboBox<VehicleStates> condition;
	@FXML
	public TextField miles;
	
	public Operator user;
	public Contract contract;
	public void initData(String id, Operator user) {
		this.user = user;
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Client.class)
				.addAnnotatedClass(Contract.class)
				.addAnnotatedClass(Operator.class)
				.addAnnotatedClass(Protocol.class)
				.addAnnotatedClass(Vehicle.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			contract = session.get(Contract.class, Integer.parseInt(id));
			session.getTransaction().commit();
		} finally {
			factory.close();
		}
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Platform.runLater(() -> {
			try {
				firstProtocol.setText(Files.readString(Paths.get(contract.getProtocol().getFileName() + ".txt")));
			} catch (IOException e) {
				e.printStackTrace();
			}
			condition.getItems().addAll(VehicleStates.Excellent,
					VehicleStates.Good,
					VehicleStates.Fair,
					VehicleStates.Poor);
		});
	}
	
	public void nextClick() throws IOException {
		if(secondProtocol.getText().isBlank() || condition.getSelectionModel().getSelectedItem() == null || !Validations.isValidInt(miles)) {
			Alert alert = new Alert(AlertType.ERROR, "Please fill all fields!", ButtonType.OK);
			alert.show();
		}
		else {
			List<Float> costs = user.returnVehicle(contract.getContractId(), secondProtocol.getText(), Integer.parseInt(miles.getText()), condition.getSelectionModel().getSelectedItem());
			
			Stage window = (Stage)firstProtocol.getScene().getWindow();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("return_car_3.fxml"));
			Parent root = loader.load();
			ReturnCarController3 controller = loader.getController();
			controller.initData(costs);
			window.setScene(new Scene(root));
		}
	}
}
