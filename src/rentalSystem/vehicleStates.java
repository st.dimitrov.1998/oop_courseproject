package rentalSystem;

public enum VehicleStates {
	Excellent,
	Good,
	Fair,
	Poor
}
