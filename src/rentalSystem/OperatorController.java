package rentalSystem;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class OperatorController implements Initializable {
	@FXML
	public ListView<String> listView;
	@FXML
	public Label name;
	
	private Operator user;
	public static String filterMake;
	public static String filterModel;
	public static VehicleClasses filterClass;
	public static VehicleCategories filterCategory;
	public static VehicleFuels filterFuel;
	
	public void initialize(URL arg0, ResourceBundle arg1) {
		Platform.runLater(() -> {
			name.setText(user.getFirstName());
			try {
				user.overdueWarning();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	public void initData(Operator user) {
		this.user = user;
	}

	public void regClientClick() throws Exception {
		Stage window = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("reg_client.fxml"));
		Parent root = loader.load();
		RegClientController controller = loader.getController();
		controller.initData(user);
		window.setScene(new Scene(root));
		window.initModality(Modality.APPLICATION_MODAL);
		window.showAndWait();
	}
	
	public void rentVehicleClick() throws Exception {
		Stage window = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("rent_car.fxml"));
		Parent root = loader.load();
		RentCarController controller = loader.getController();
		controller.initData(user);
		window.setScene(new Scene(root));
		window.initModality(Modality.APPLICATION_MODAL);
		window.showAndWait();
	}
	
	public void returnVehicleClick() throws Exception {
		Stage window = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("return_car_1.fxml"));
		Parent root = loader.load();
		ReturnCarController1 controller = loader.getController();
		controller.initData(user);
		window.setScene(new Scene(root));
		window.initModality(Modality.APPLICATION_MODAL);
		window.showAndWait();
	}
	
	public void listAvailableVehiclesClick() {
		listView.getItems().clear();
		for(Vehicle vehicle : user.getAvailableVehicles()) {
			listView.getItems().add(vehicle.toString());
		}
	}

	public void listRentalHistoryClick() {
		listView.getItems().clear();
		for(Contract contract : user.getRentalHistory()) {
			listView.getItems().add(contract.toString());
		}
	}

	public void listWorkReportClick() {
		listView.getItems().clear();
		for(Operator operator : user.getAllOperators()) {
			listView.getItems().add(operator.toString());
		}
		listView.setOnMouseClicked(e -> {
			int id = Integer.parseInt(listView.getSelectionModel().getSelectedItem().split(" ")[0]);
			listView.getItems().clear();
			for(Contract contract : user.getWorkReport(id)) {
				listView.getItems().add(contract.toString());
			}
		});
	}
	
	public void listRentedStatsClick() throws Exception {
		listView.getItems().clear();
		
		Stage window = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("query_filters.fxml"));
		Parent root = loader.load();
		window.setScene(new Scene(root));
		window.initModality(Modality.APPLICATION_MODAL);
		window.showAndWait();
		
		for(Vehicle vehicle : user.getRentedVehicleStats(filterMake, filterModel, filterClass, filterCategory, filterFuel)) {
			listView.getItems().add(vehicle.toString());
		}
	}
	
	public void listClientRatingsClick() {
		listView.getItems().clear();
		for(Client client : user.getAllClients("")) {
			listView.getItems().add(client.getPid()+" | "+client.getFirstName()+" "+client.getLastName()+" \t| Rating: "+client.getRating());
		}
	}
	
	@FXML
	public void logOut() throws Exception {
		user.logOut();
	}
}
