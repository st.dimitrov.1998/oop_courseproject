package rentalSystem;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class ReturnCarController1 implements Initializable {
	@FXML
	public ListView<String> contract;
	
	public Operator user;
	public void initData(Operator user) {
		this.user = user;
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Platform.runLater(() -> {
			for(Contract contract : user.getActiveContracts())
				this.contract.getItems().add(contract.getContractId() + " | " + contract.getDateOfRent().toString() + " -- " + contract.getVehicle().toString());
		});
	}
	
	public void nextClick() throws Exception {
		if(contract.getSelectionModel().getSelectedItem() != null) {
			String id = contract.getSelectionModel().getSelectedItem().split(" ")[0];
			
			Stage window = (Stage)contract.getScene().getWindow();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("return_car_2.fxml"));
			Parent root = loader.load();
			ReturnCarController2 controller = loader.getController();
			controller.initData(id, user);
			window.setScene(new Scene(root));
		}
		else{
			Alert alert = new Alert(AlertType.ERROR, "Please select a contract!", ButtonType.OK);
			alert.show();
		}
	}
}
