package rentalSystem;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import javafx.stage.Stage;

@Entity
@Table(name="users")
public class Admin extends User{

	public Admin() {
		super();
	}

	public Admin(String password, String firstName, String lastName, String phoneNumber) {
		super(password, firstName, lastName, phoneNumber);
	}
	
	public void logIn(String pass) throws Exception {
		if(this.getPassword().equals(pass)) {
			SessionFactory factory = new Configuration()
					.configure()
					.addAnnotatedClass(Admin.class)
					.buildSessionFactory();
			
			Session session = factory.getCurrentSession();
			try {
				session.beginTransaction();
				session.createQuery("update Admin set loggedIn=1 where id="+getId()).executeUpdate();
				session.getTransaction().commit();
			} finally {
				factory.close();
			}
			Stage window = Main.getMainStage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("admin.fxml"));
			Parent root = loader.load();
			AdminController controller = loader.getController();
			controller.initData(this);
			window.setTitle("Rental System - Administrator panel");
			window.setScene(new Scene(root));
			window.setMaximized(true);
			window.setOnCloseRequest(e -> {
				try {
					logOut();
				} catch (Exception exc) {
					exc.printStackTrace();
				}
			});
			window.show();
		}
		else {
			Alert alert = new Alert(AlertType.ERROR, "Incorrect password!", ButtonType.OK);
			alert.show();
		}
	}
	
	public void registerCompany() throws Exception {
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Company.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			if(session.get(Company.class, 0) == null) {
				Stage window = new Stage();
				FXMLLoader loader = new FXMLLoader(getClass().getResource("reg_company.fxml"));
				Parent root = loader.load();
				window.setScene(new Scene(root));
				window.initModality(Modality.APPLICATION_MODAL);
				window.showAndWait();
			}
			session.getTransaction().commit();
		} finally {
			factory.close();
		}
	}
	
	public void registerOperator(String pass, String fName, String lName, String phoneNo) throws Exception {
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Operator.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			Operator operator = new Operator(pass,fName,lName,phoneNo);
			session.save(operator);
			session.getTransaction().commit();
		} finally {
			factory.close();
		}
	}
	
	public void registerVehicle(String vin, String plate, VehicleStates state, String make, String model, int year, VehicleClasses vClass,
			VehicleCategories category, String colour, float displ, int power, VehicleFuels fuel, int mileage, boolean smokingAllowed) {	
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Vehicle.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			Vehicle vehicle = new Vehicle(vin, plate, state, make, model, year, vClass, category, colour, displ, power, fuel, mileage, smokingAllowed);
			session.save(vehicle);
			session.getTransaction().commit();
		} finally {
			factory.close();
		}
	}
	
	public void editVehicle(String vin, String plate, VehicleStates state, String make, String model, int year, 
			VehicleClasses vClass, VehicleCategories category, String colour, float displ, int power, VehicleFuels fuel, int mileage, 
			boolean smokingAllowed) {
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Vehicle.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			Vehicle vehicle = session.get(Vehicle.class, vin);
			vehicle.setLicensePlate(plate);
			vehicle.setMake(make);
			vehicle.setModel(model);
			vehicle.setYear(year);
			vehicle.setColour(colour);
			vehicle.setHorsepower(power);
			vehicle.setMileage(mileage);
			vehicle.setCurrentState(state);
			vehicle.setVehicleClass(vClass);
			vehicle.setVehicleCategory(category);
			vehicle.setVehicleFuel(fuel);
			if(fuel == VehicleFuels.Electric)
				vehicle.setEngineSize(0f);
			else
				vehicle.setEngineSize(displ);
			vehicle.setSmokingAllowed(smokingAllowed);
			if(state == VehicleStates.Poor)
				vehicle.setAvailable(false);
			else
				vehicle.setAvailable(true);
			session.getTransaction().commit();
		} finally {
			factory.close();
		}
	}

	public void logOut() throws Exception {
		Stage window = Main.getMainStage();
		Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
		window.setTitle("Rental System");
		window.setMaximized(false);
		window.setScene(new Scene(root,225,210));
		window.show();
		
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Admin.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			session.createQuery("update Admin set loggedIn=0 where id="+getId()).executeUpdate();
			session.getTransaction().commit();
		} finally {
			factory.close();
		}
	}
}