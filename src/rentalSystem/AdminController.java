package rentalSystem;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AdminController implements Initializable {
	@FXML
	public ListView<String> listView;
	
	private Admin user;
	public static String filterMake;
	public static String filterModel;
	public static VehicleClasses filterClass;
	public static VehicleCategories filterCategory;
	public static VehicleFuels filterFuel;
	
	public void initData(Admin user) {
		this.user = user;
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Platform.runLater(() -> {
			try {
				user.registerCompany();
				user.overdueWarning();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	@FXML
	public void regOperatorClick() throws Exception {
		Stage window = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("reg_operator.fxml"));
		Parent root = loader.load();
		RegOperatorController controller = loader.getController();
		controller.initData(user);
		window.setScene(new Scene(root));
		window.initModality(Modality.APPLICATION_MODAL);
		window.showAndWait();
	}
	
	@FXML
	public void regVehicleClick() throws Exception {
		Stage window = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("reg_edit_vehicle.fxml"));
		RegVehicleController controller = new RegVehicleController();
		loader.setController(controller);
		controller.initData(user);
		Parent root = loader.load();
		window.setScene(new Scene(root));
		window.initModality(Modality.APPLICATION_MODAL);
		window.showAndWait();
	}

	@FXML
	public void editVehicleClick() {
		listView.getItems().clear();
		for(Vehicle vehicle : user.getAllVehicles()) {
			listView.getItems().add(vehicle.toString());
		}
		listView.setOnMouseClicked(e -> {
			try {
				Stage window = new Stage();
				FXMLLoader loader = new FXMLLoader(getClass().getResource("reg_edit_vehicle.fxml"));
				EditVehicleController controller = new EditVehicleController();
				loader.setController(controller);
				controller.initData(user, listView.getSelectionModel().getSelectedItem().split(" ")[0]);
				Parent root = loader.load();
				window.setScene(new Scene(root));
				window.initModality(Modality.APPLICATION_MODAL);
				window.showAndWait();
			} catch (IOException exc) {
				exc.printStackTrace();
			}
		});
	}

	public void listAvailableVehiclesClick() {
		listView.getItems().clear();
		for(Vehicle vehicle : user.getAvailableVehicles()) {
			listView.getItems().add(vehicle.toString());
		}
	}

	public void listRentalHistoryClick() {
		listView.getItems().clear();
		for(Contract contract : user.getRentalHistory()) {
			listView.getItems().add(contract.toString());
		}
	}

	public void listWorkReportClick() {
		listView.getItems().clear();
		for(Operator operator : user.getAllOperators()) {
			listView.getItems().add(operator.toString());
		}
		listView.setOnMouseClicked(e -> {
			int id = Integer.parseInt(listView.getSelectionModel().getSelectedItem().split(" ")[0]);
			listView.getItems().clear();
			for(Contract contract : user.getWorkReport(id)) {
				listView.getItems().add(contract.toString());
			}
		});
	}
	
	public void listRentedStatsClick() throws Exception {
		listView.getItems().clear();
		
		Stage window = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("query_filters.fxml"));
		Parent root = loader.load();
		window.setScene(new Scene(root));
		window.initModality(Modality.APPLICATION_MODAL);
		window.showAndWait();
		
		for(Vehicle vehicle : user.getRentedVehicleStats(filterMake, filterModel, filterClass, filterCategory, filterFuel)) {
			listView.getItems().add(vehicle.toString());
		}
	}
	
	public void listClientRatingsClick() {
		listView.getItems().clear();
		for(Client client : user.getAllClients("")) {
			listView.getItems().add(client.getPid()+" | "+client.getFirstName()+" "+client.getLastName()+" \t| Rating: "+client.getRating());
		}
	}
	
	@FXML
	public void logOut() throws Exception {
		user.logOut();
	}
}
