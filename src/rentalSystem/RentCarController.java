package rentalSystem;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class RentCarController implements Initializable {
	@FXML
	public TextField pid;
	@FXML 
	public ListView<String> client;
	@FXML
	public ListView<String> vehicle;
	@FXML
	public TextArea protocol;
	@FXML
	public DatePicker date;
	
	public Operator user;
	public void initData(Operator user) {
		this.user = user;
	}
	
	public void initialize(URL arg0, ResourceBundle arg1) {
		Platform.runLater(() -> {
			for(Client client : user.getAllClients(""))
				this.client.getItems().add(client.toString());
			for(Vehicle vehicle : user.getAvailableVehicles())
				this.vehicle.getItems().add(vehicle.toString());
			date.setValue(LocalDate.now().plusDays(7));
		});
	}
	
	public void pidKeyTyped() {
		client.getItems().clear();
		for(Client client : user.getAllClients(pid.getText())) {
			this.client.getItems().add(client.toString());
		}
	}
	
	public void submitClick() throws IOException {
		if(client.getSelectionModel().getSelectedItem() == null || client.getSelectionModel().getSelectedItem() == null || protocol.getText().isBlank()){
			Alert alert = new Alert(AlertType.ERROR, "Please fill all fields!", ButtonType.OK);
			alert.show();
		}
		else if(date.getValue().isBefore(LocalDate.now().plusDays(1))) {
			Alert alert = new Alert(AlertType.ERROR, "The date you have entered is invalid!", ButtonType.OK);
			alert.show();
		}
		else {
			String strPid = client.getSelectionModel().getSelectedItem().split(" ")[0];
			String strVin = vehicle.getSelectionModel().getSelectedItem().split(" ")[0];
			user.rentOutVehicle(strPid, strVin, protocol.getText(), date.getValue());
			Stage currentStage = (Stage)pid.getScene().getWindow();
			currentStage.close();
		}
	}
}
