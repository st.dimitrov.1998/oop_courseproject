package rentalSystem;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

@Entity
@Table(name="users")
public class Operator extends User{
	
	public Operator() {
		super();
	}

	public Operator(String password, String firstName, String lastName, String phoneNumber) {
		super(password, firstName, lastName, phoneNumber);
	}
	
	public String toString() {
		return getId()+" | "+getFirstName()+" "+getLastName()+" | "+getPhoneNumber();
	}
	
	public void logIn(String pass) throws Exception {
		if(this.getPassword().equals(pass)) {
			SessionFactory factory = new Configuration()
					.configure()
					.addAnnotatedClass(Operator.class)
					.buildSessionFactory();
			
			Session session = factory.getCurrentSession();
			try {
				session.beginTransaction();
				session.createQuery("update Operator set loggedIn=1 where id="+getId()).executeUpdate();
				session.getTransaction().commit();
			} finally {
				factory.close();
			}
			Stage window = Main.getMainStage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("operator.fxml"));
			Parent root = loader.load();
			OperatorController controller = loader.getController();
			controller.initData(this);
			window.setTitle("Rental System - Operator panel");
			window.setScene(new Scene(root));
			window.setMaximized(true);
			window.setOnCloseRequest(e -> {
				try {
					logOut();
				} catch (Exception exc) {
					exc.printStackTrace();
				}
			});
			window.show();
		}
		else{
			Alert alert = new Alert(AlertType.ERROR, "Incorrect password!", ButtonType.OK);
			alert.show();
		}
	}
	
	public void registerClient(String pid, String fName, String lName, String phoneNo, String address, String iban) {
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Client.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			Client client = new Client(pid, fName, lName, phoneNo, address, iban);
			session.save(client);
			session.getTransaction().commit();
		} finally {
			factory.close();
		}
	}
	
	public void rentOutVehicle(String pid, String vin, String protocolText, LocalDate date) throws IOException{
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Contract.class)
				.addAnnotatedClass(Operator.class)
				.addAnnotatedClass(Vehicle.class)
				.addAnnotatedClass(Protocol.class)
				.addAnnotatedClass(Client.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			
			Client client = session.get(Client.class,  pid);
			if(checkClientRating(client)) {
				Vehicle vehicle = session.get(Vehicle.class,  vin);
				Protocol protocol = new Protocol("protocol-" + vehicle.getVin() + "-" + date.format(DateTimeFormatter.ofPattern("dd-MMM-yy")));
				FileWriter file = new FileWriter(protocol.getFileName() + ".txt");
				file.write(protocolText);
				file.close();
				session.save(protocol);
				Contract newContract = new Contract(this, client, vehicle, protocol, date);
				session.save(newContract);
				session.createQuery("update Vehicle set available=0 where vin='"+vehicle.getVin()+"'").executeUpdate();
			}
			session.getTransaction().commit();
		} finally {
			factory.close();
		}
	}
	
	public List<Float> returnVehicle(int id, String protocolText, int milesDone, VehicleStates newState) throws IOException{
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Client.class)
				.addAnnotatedClass(Contract.class)
				.addAnnotatedClass(Operator.class)
				.addAnnotatedClass(Protocol.class)
				.addAnnotatedClass(Vehicle.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			Contract contract = session.get(Contract.class, id);
			FileWriter file = new FileWriter(contract.getProtocol().getFileName() + ".txt", true);
			file.write("\n\n\t-- UPON RETURN --\n\n" + protocolText);
			file.close();
			boolean violation = false;
			switch(contract.getVehicle().getCurrentState()) {
			case Excellent:
				if(newState != VehicleStates.Excellent) {
					contract.getClient().lowerRating();
					violation = true;
				}
				break;
			case Fair:
				if(newState != VehicleStates.Excellent && newState != VehicleStates.Fair) {
					contract.getClient().lowerRating();
					violation = true;
				}
				break;
			case Good:
				if(newState == VehicleStates.Poor) {
					contract.getClient().lowerRating();
					violation = true;
				}
				break;
			}
			boolean maintenance = false;
			if(newState == VehicleStates.Poor) {
				contract.getClient().lowerRating();
				maintenance = true;
			}
			else {
				contract.getVehicle().setAvailable(true);
			}
			contract.getVehicle().setCurrentState(newState);
			contract.getVehicle().addMiles(milesDone);
			contract.setDateOfReturn(LocalDateTime.now());
			if(contract.getDateOfReturn().isAfter(contract.getDueDate().atStartOfDay())) {
				violation = true;
			}
			List<Float> costs = calculateCosts(contract.getVehicle().getVehicleClass(), 
					Math.abs(Duration.between(contract.getDateOfRent(), contract.getDateOfReturn()).toDays()), 
					milesDone, violation, maintenance);
			contract.setTotalCost(costs.get(costs.size()-1));
			session.getTransaction().commit();
			return costs;
		} finally {
			factory.close();
		}
	}
	
	public List<Float> calculateCosts(VehicleClasses vClass, long days, int miles, boolean violation, boolean maintenance) {
		List<Float> costs = new ArrayList<Float>();
		float total = 0f;
		float milesRate = 0f, dayRate = 0f, maintenanceCost = 0f;
		
		switch(vClass) {
		case Cruiser:
			milesRate = 1.29f;
			dayRate = 5f;
			maintenanceCost = 200f;
			break;
		case Family:
			milesRate = 1.50f;
			dayRate = 10f;
			maintenanceCost = 230f;
			break;
		case Sport:
			milesRate = 2.19f;
			dayRate = 18f;
			maintenanceCost = 470f;
			break;
		case Luxurious:
			milesRate = 3.99f;
			dayRate = 25f;
			maintenanceCost = 1500f;
			break;
		}
		
		total += ((float)days * dayRate) + ((float)miles * milesRate);
		
		costs.add((float) days);
		costs.add(dayRate);
		costs.add((float) miles);
		costs.add(milesRate);
		costs.add(total);
		
		if(violation) {
			costs.add(total/2f);
			total += total/2f;
		}
		
		if(maintenance) {
			costs.add(maintenanceCost);
			total += maintenanceCost;
		}
		
		costs.add(total);
		
		return costs;

	}
		
	public boolean checkClientRating(Client client) {
		if(client.getRating() < 1) {
			Alert alert = new Alert(AlertType.CONFIRMATION, "This clients rating is low. Do you still accept his rent request?", ButtonType.YES, ButtonType.NO);
			alert.showAndWait();
			if(alert.getResult() == ButtonType.NO)
				return false;
		}
		return true;
	}

	public void logOut() throws Exception {
		Stage window = Main.getMainStage();
		Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
		window.setTitle("Rental System");
		window.setMaximized(false);
		window.setScene(new Scene(root,225,210));
		window.show();
		
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Operator.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			session.createQuery("update Operator set loggedIn=0 where id="+getId()).executeUpdate();
			session.getTransaction().commit();
		} finally {
			factory.close();
		}
	}
}