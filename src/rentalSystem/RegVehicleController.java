package rentalSystem;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;;

public class RegVehicleController implements Initializable {
	@FXML
	public TextField vin;
	@FXML
	public TextField plate;
	@FXML
	public ComboBox<VehicleStates> state;
	@FXML
	public TextField make;
	@FXML
	public TextField model;
	@FXML
	public TextField year;
	@FXML
	public ComboBox<VehicleClasses> vClass;
	@FXML
	public ComboBox<VehicleCategories> category;
	@FXML
	public TextField colour;
	@FXML
	public TextField displ;
	@FXML
	public TextField power;
	@FXML
	public ComboBox<VehicleFuels> fuel;
	@FXML
	public TextField mileage;
	@FXML
	public CheckBox smokingAllowed;
	public Admin user;
	
	public void initData(Admin user) {
		this.user = user;
	}
	
	public void submitClick() throws Exception {
		if(!Validations.isValidVIN(vin) || !Validations.isValidLicense(plate) || !Validations.isNonNumeric(make) || model.getText().isBlank() 
				|| !Validations.isValidYear(year) || !Validations.isNonNumeric(colour) || !Validations.isValidFloat(displ) || !Validations.isValidInt(power) 
				|| !Validations.isValidInt(mileage) || state.getSelectionModel().isEmpty() || vClass.getSelectionModel().isEmpty() 
				|| category.getSelectionModel().isEmpty() || fuel.getSelectionModel().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR, "Please fill all fields!", ButtonType.OK);
			alert.show();
		}
		else {
			user.registerVehicle(vin.getText(), plate.getText(), state.getSelectionModel().getSelectedItem(), make.getText(), model.getText(), 
					Integer.parseInt(year.getText()), vClass.getSelectionModel().getSelectedItem(), category.getSelectionModel().getSelectedItem(), 
					colour.getText(), Float.parseFloat(displ.getText()), Integer.parseInt(power.getText()), fuel.getSelectionModel().getSelectedItem(),
					Integer.parseInt(mileage.getText()), smokingAllowed.isSelected());
			Stage currentStage = (Stage)vin.getScene().getWindow();
			currentStage.close();
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		state.getItems().addAll(VehicleStates.Excellent,
				VehicleStates.Good,
				VehicleStates.Fair,
				VehicleStates.Poor);
		
		vClass.getItems().addAll(VehicleClasses.Luxurious,
				VehicleClasses.Sport,
				VehicleClasses.Cruiser,
				VehicleClasses.Family);
		
		category.getItems().addAll(VehicleCategories.Convertible,
				VehicleCategories.Coupe,
				VehicleCategories.Estate,
				VehicleCategories.Hatchback,
				VehicleCategories.Minivan,
				VehicleCategories.Pickup,
				VehicleCategories.Roadster,
				VehicleCategories.Sedan,
				VehicleCategories.SUV,
				VehicleCategories.Targa);
		
		fuel.getItems().addAll(VehicleFuels.Gasoline,
				VehicleFuels.Diesel,
				VehicleFuels.Propane,
				VehicleFuels.CNG,
				VehicleFuels.Ethanol,
				VehicleFuels.Electric);
	}
}
