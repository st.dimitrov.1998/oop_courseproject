package rentalSystem;

public enum VehicleCategories {
	Convertible,
	Coupe,
	Estate,
	Hatchback,
	Minivan,
	Pickup,
	Roadster,
	Sedan,
	SUV,
	Targa
}
