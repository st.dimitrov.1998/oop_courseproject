package rentalSystem;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="protocols")
public class Protocol {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="protocol_id")
	private int id;
	
	@Column(name="file_name")
	private String fileName;

	public Protocol() {
	}

	public Protocol(String fileName) {
		this.fileName = fileName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
