package rentalSystem;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

@MappedSuperclass
abstract public class User {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id")
	private int id;
	
	@Column(name="password")
	private String password;
	
	@Column(name="logged_in")
	private boolean loggedIn;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="phone_number")
	private String phoneNumber;

	public User() {
	}

	public User(String password, String firstName, String lastName, String phoneNumber) {
		this.password = password;
		this.loggedIn = false;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneNumber = phoneNumber;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getFullName() {
		return firstName + " " + lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public List<Vehicle> getAvailableVehicles(){
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Vehicle.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			List<Vehicle> theVehicles = session.createQuery("from Vehicle where available = 1").list();
			session.getTransaction().commit();
			
			return theVehicles;
		} finally {
			factory.close();
		}
	}
	
	public List<Contract> getRentalHistory(){
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Admin.class)
				.addAnnotatedClass(Client.class)
				.addAnnotatedClass(Contract.class)
				.addAnnotatedClass(Operator.class)
				.addAnnotatedClass(Protocol.class)
				.addAnnotatedClass(Vehicle.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			List<Contract> rentalHistory = session.createQuery("from Contract").list();
			session.getTransaction().commit();
			
			return rentalHistory;
		} finally {
			factory.close();
		}
	}
	
	public List<Contract> getWorkReport(int id){
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Admin.class)
				.addAnnotatedClass(Client.class)
				.addAnnotatedClass(Contract.class)
				.addAnnotatedClass(Operator.class)
				.addAnnotatedClass(Protocol.class)
				.addAnnotatedClass(Vehicle.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			List<Contract> workReport = session.createQuery("from Contract where operator = " + id).list();
			session.getTransaction().commit();
			
			return workReport;
		} finally {
			factory.close();
		}
	}
	
	public List<Client> getAllClients(String filter){
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Client.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			List<Client> theClients = session.createQuery("from Client where pid like '" + filter + "%'").list();
			session.getTransaction().commit();
			
			return theClients;
		} finally {
			factory.close();
		}
	}
	
	public List<Vehicle> getRentedVehicleStats(String make, String model, VehicleClasses vehicleClass, 
			VehicleCategories vehicleCategory, VehicleFuels vehicleFuel){
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Vehicle.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			String query = "from Vehicle where available = 0 AND currentState != '"+VehicleStates.Poor+"'";
			if(make.length()!=0) query+=" AND make LIKE '%"+make+"%'";
			if(model.length()!=0) query+=" AND model LIKE '%"+model+"%'";
			if(vehicleClass!=null) query+=" AND vehicleClass LIKE '"+vehicleClass+"'";
			if(vehicleCategory!=null) query+=" AND vehicleCategory LIKE '"+vehicleCategory+"'";
			if(vehicleFuel!=null) query+=" AND vehicleFuel LIKE '"+vehicleFuel+"'";
			
			session.beginTransaction();
			List<Vehicle> theVehicles = session.createQuery(query).list();
			session.getTransaction().commit();
			
			return theVehicles;
		} finally {
			factory.close();
		}
	}
	
	public List<Vehicle> getAllVehicles(){
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Vehicle.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			List<Vehicle> theVehicles = session.createQuery("from Vehicle").list();
			session.getTransaction().commit();
			
			return theVehicles;
		} finally {
			factory.close();
		}
	}

	public List<Contract> getActiveContracts(){
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Admin.class)
				.addAnnotatedClass(Client.class)
				.addAnnotatedClass(Contract.class)
				.addAnnotatedClass(Operator.class)
				.addAnnotatedClass(Protocol.class)
				.addAnnotatedClass(Vehicle.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			List<Contract> theContracts = session.createQuery("from Contract where dateOfReturn = NULL").list();
			session.getTransaction().commit();
			
			return theContracts;
		} finally {
			factory.close();
		}
	}
	
	public List<Operator> getAllOperators(){
		SessionFactory factory = new Configuration()
				.configure()
				.addAnnotatedClass(Operator.class)
				.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		try {
			session.beginTransaction();
			List<Operator> theOperators = session.createQuery("from Operator").list();
			theOperators.remove(0);
			session.getTransaction().commit();
			
			return theOperators;
		} finally {
			factory.close();
		}
	}
	
	public void overdueWarning() throws Exception {
		List<Contract> contracts = new ArrayList<Contract>();
		for(Contract c : getActiveContracts()) {
			if(LocalDate.now().isAfter(c.getDueDate().minusDays(2)))
				contracts.add(c);
		}
		if(contracts.isEmpty())
			return;
		
		Stage window = new Stage();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("overdue_warning.fxml"));
		Parent root = loader.load();
		OverdueWarningController controller = loader.getController();
		controller.initData(contracts);
		window.setScene(new Scene(root));
		window.showAndWait();
	}

}
