package rentalSystem;

public enum VehicleFuels {
	Gasoline,
	Diesel,
	Propane,
	CNG,
	Ethanol,
	Electric
}
