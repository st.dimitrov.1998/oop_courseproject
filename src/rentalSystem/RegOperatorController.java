package rentalSystem;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class RegOperatorController {
	@FXML
	public TextField fName;
	@FXML
	public TextField lName;
	@FXML
	public TextField pass;
	@FXML
	public TextField phoneNo;
	
	public Admin user;
	
	public void initData(Admin user) {
		this.user = user;
	}
	
	@FXML
	public void submitClick() throws Exception {
		if(!Validations.isNonNumeric(fName) || !Validations.isNonNumeric(lName) || pass.getText().isBlank() || !Validations.isValidInt(phoneNo)) {
			Alert alert = new Alert(AlertType.ERROR, "Please fill all fields!", ButtonType.OK);
			alert.show();
		}
		else {
			user.registerOperator(pass.getText(), fName.getText(), lName.getText(), phoneNo.getText());
			Stage currentStage = (Stage)fName.getScene().getWindow();
			currentStage.close();
		}
	}
}
