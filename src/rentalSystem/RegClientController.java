package rentalSystem;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class RegClientController {
	@FXML
	public TextField fName;
	@FXML
	public TextField lName;
	@FXML
	public TextField pid;
	@FXML
	public TextField address;
	@FXML
	public TextField phoneNo;
	@FXML
	public TextField iban;
	
	public Operator user;
	public void initData(Operator user) {
		this.user = user;
	}
	
	public void submitClick() {
		if(!Validations.isNonNumeric(fName) || !Validations.isNonNumeric(lName) || !Validations.isValidPID(pid) || address.getText().isBlank() || !Validations.isValidInt(phoneNo) || iban.getText().isBlank()){
			Alert alert = new Alert(AlertType.ERROR, "Please fill all fields!", ButtonType.OK);
			alert.show();
		}
		else {
			user.registerClient(pid.getText(), fName.getText(), lName.getText(), phoneNo.getText(), address.getText(), iban.getText());
			Stage currentStage = (Stage)fName.getScene().getWindow();
			currentStage.close();
		}
	}
}
