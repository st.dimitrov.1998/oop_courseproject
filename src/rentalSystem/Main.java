package rentalSystem;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class Main extends Application {
	
	private static Stage mainStage;
	
	public void start(Stage primaryStage) throws Exception{
    	Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
    	primaryStage.setTitle("Rental System");
    	primaryStage.setScene(new Scene(root));
    	mainStage = primaryStage;
    	primaryStage.show();
    }
    
    public static void main(String[] args) {
        Application.launch(args);
    }
    
    public static Stage getMainStage() {
    	return mainStage;
    }
}
