package rentalSystem;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

public class ReturnCarController3 implements Initializable {
	@FXML
	public Label heading;
	@FXML
	public ListView<String> bill;
	
	public List<Float> costs = new ArrayList<Float>();
	public void initData(List<Float> costs) {
		this.costs = costs;
	}
	
	public void initialize(URL arg0, ResourceBundle arg1) {
		Platform.runLater(() -> {
			heading.setText("TOTAL COST: "+costs.get(costs.size()-1)+"$");
			bill.getItems().add("Rent cost\t\t\t\t\t\t"+costs.get(4)+"$");
			bill.getItems().add(" "+costs.get(0)+" days X "+costs.get(1)+"$");
			bill.getItems().add(" "+costs.get(2)+" mi X "+costs.get(3)+"$");
			if(costs.size()>6) {
				bill.getItems().add("");
				bill.getItems().add("Contract violation\t\t\t\t"+costs.get(5)+"$");
				bill.getItems().add(" (50% of rent cost)");
			}
			
			if(costs.size()==8) {
				bill.getItems().add("");
				bill.getItems().add("Maintenance Cost\t\t\t\t"+costs.get(6)+"$");
			}
			bill.getItems().add("");
			bill.getItems().add("Total cost\t\t\t\t\t"+costs.get(costs.size()-1)+"$");
		});
	}
	
	public void okClick() {
		Stage stage = (Stage)heading.getScene().getWindow();
		stage.close();
	}
}
