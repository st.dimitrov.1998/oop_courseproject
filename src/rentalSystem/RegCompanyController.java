package rentalSystem;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class RegCompanyController {
	@FXML
	public TextField name;
	@FXML
	public TextField phoneNo;
	@FXML
	public TextField address;
	@FXML
	public TextField email;
	
	public void submitClick() {
		if(!Validations.isNonNumeric(name) || !Validations.isValidInt(phoneNo) || address.getText().isBlank() || !Validations.isValidEmail(email)){
			Alert alert = new Alert(AlertType.ERROR, "Please fill all fields!", ButtonType.OK);
			alert.show();
		}
		else {
			SessionFactory factory = new Configuration()
					.configure()
					.addAnnotatedClass(Company.class)
					.buildSessionFactory();
			
			Session session = factory.getCurrentSession();
			try {
				session.beginTransaction();
				Company company = new Company(name.getText(), address.getText(), phoneNo.getText(), email.getText());
				session.save(company);
				session.getTransaction().commit();
				Stage stage = (Stage)name.getScene().getWindow();
				stage.close();
			} finally {
				factory.close();
			}
		}
	}
}
