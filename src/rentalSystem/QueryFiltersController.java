package rentalSystem;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class QueryFiltersController implements Initializable {
	@FXML
	public TextField make;
	@FXML
	public TextField model;
	@FXML
	public ComboBox<VehicleClasses> vClass;
	@FXML
	public ComboBox<VehicleCategories> category;
	@FXML
	public ComboBox<VehicleFuels> fuel;
	
	@FXML
	public void submitClick() {
		AdminController.filterMake = make.getText();
		AdminController.filterModel = model.getText();
		AdminController.filterClass = vClass.getSelectionModel().getSelectedItem();
		AdminController.filterCategory = category.getSelectionModel().getSelectedItem();
		AdminController.filterFuel = fuel.getSelectionModel().getSelectedItem();
		OperatorController.filterMake = make.getText();
		OperatorController.filterModel = model.getText();
		OperatorController.filterClass = vClass.getSelectionModel().getSelectedItem();
		OperatorController.filterCategory = category.getSelectionModel().getSelectedItem();
		OperatorController.filterFuel = fuel.getSelectionModel().getSelectedItem();
		
		Stage currentStage = (Stage)make.getScene().getWindow();
		currentStage.close();
	}
		
	public void initialize(URL arg0, ResourceBundle arg1) {
		vClass.getItems().addAll(VehicleClasses.Luxurious,
				VehicleClasses.Sport,
				VehicleClasses.Cruiser,
				VehicleClasses.Family);
		
		category.getItems().addAll(VehicleCategories.Convertible,
				VehicleCategories.Coupe,
				VehicleCategories.Estate,
				VehicleCategories.Hatchback,
				VehicleCategories.Minivan,
				VehicleCategories.Pickup,
				VehicleCategories.Roadster,
				VehicleCategories.Sedan,
				VehicleCategories.SUV,
				VehicleCategories.Targa);
		
		fuel.getItems().addAll(VehicleFuels.Gasoline,
				VehicleFuels.Diesel,
				VehicleFuels.Propane,
				VehicleFuels.CNG,
				VehicleFuels.Ethanol,
				VehicleFuels.Electric);
	}	
}
