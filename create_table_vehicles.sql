USE rental_system;

DROP TABLE IF EXISTS `vehicles`;

CREATE TABLE `vehicles` (
  `vin` varchar(17) NOT NULL,
  `license_plate` varchar(7) NOT NULL UNIQUE,
  `available` boolean DEFAULT TRUE,
  `current_state` enum("Excellent","Good","Fair","Poor") DEFAULT NULL,
  `make` varchar(40) DEFAULT NULL,
  `model` varchar(40) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `class` enum("Luxurious","Sport","Cruiser","Family") DEFAULT NULL,
  `category` enum("Convertible","Coupe","Estate","Hatchback","Minivan","Pickup","Roadster","Sedan","SUV","Targa") DEFAULT NULL,
  `colour` varchar(40) DEFAULT NULL,
  `engine_size` float(2,1) DEFAULT NULL,
  `horsepower` smallint DEFAULT NULL,
  `fuel` enum("Gasoline","Diesel","Propane","CNG","Ethanol","Electric") DEFAULT NULL,
  `mileage` int DEFAULT NULL,
  `smoking_allowed` boolean DEFAULT FALSE,
  PRIMARY KEY (`vin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;