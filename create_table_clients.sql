USE rental_system;

DROP TABLE IF EXISTS clients;

CREATE TABLE clients(
	`client_pid` varchar(10) NOT NULL,
    `first_name` varchar(50) DEFAULT NULL,
    `last_name` varchar(50) DEFAULT NULL,
    `phone_number` varchar(10) DEFAULT NULL,
    `address` varchar(50) DEFAULT NULL,
    `billing_info` varchar(34) DEFAULT NULL,
    `rating` float(3,2) DEFAULT NULL,
    PRIMARY KEY(`client_pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;