USE rental_system;

DROP TABLE IF EXISTS protocols;

CREATE TABLE protocols(
    `protocol_id` int NOT NULL AUTO_INCREMENT,
    `file_name` varchar(255) DEFAULT NULL,
    PRIMARY KEY(`protocol_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;