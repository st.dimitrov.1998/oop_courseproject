USE rental_system;

DROP TABLE IF EXISTS users;

CREATE TABLE users(
	`user_id` int NOT NULL AUTO_INCREMENT,
    `password` varchar(24) DEFAULT NULL,
    `logged_in` boolean DEFAULT FALSE,
    `first_name` varchar(50) DEFAULT NULL,
    `last_name` varchar(50) DEFAULT NULL,
    `phone_number` varchar(10) DEFAULT NULL,
    PRIMARY KEY(`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;